<img align="right" width="400" src="https://github-readme-stats.vercel.app/api?username=anis3139&show_icons=true&count_private=true" alt="Arafat's Github Stats"/>


# Hello, I'm Anichur Rahaman...
Full-stack web developer from Dhaka, Bangladesh. I have rich experience in laravel and vuejs based web application development, also I am good at WordPress. Using PHP as primary backend language since 2017. Have working experience in python and React JS also.
Currently, working as a **Software Engineer** in **[weDevs](https://wedevs.com/about/team)** ([Appsero](https://appsero.com) Project).

## Technologies
- PHP - Larvel, Wordpress...
- Javascript - VueJS, NuxtJS, jQuery, React... 
- Python
- HTML
- CSS - Boostrap, Tailwind...
- Linux, Docker, nginx...
- GCP - Datastore, BigQuery, Dataflow, Cloud Storage... 
- cPanel, WHM, VPS, Dedicated...

and more...
 
## Connect with me

<p align="left">
<a href="https://twitter.com/anis3139" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="anis3139" height="30" width="40" /></a>
<a href="https://linkedin.com/in/anis3139" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="anis3139" height="30" width="40" /></a>
 <a href="https://facebook.com/anis3139" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="anis3139" height="30" width="40" /></a>
 
</p>

## Support

<p><a href="https://www.buymeacoffee.com/anis3139" target="_blank"> <img align="left" src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="anis3139" /></a></p>

<br><br><br>

<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=anis3139&langs_count=10&show_icons=true&locale=en&layout=compact&count_private=true" alt="anis3139" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=anis3139&count_private=true" alt="anis3139" /></p>
